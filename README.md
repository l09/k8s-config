# L09

Pericia propose L09, un service offrant une gamme d'applications libres hébergées spécifiquement pour les résidents de 
l'Ariège. Ce service vise à fournir des outils numériques gratuits sans compromettre la sécurité ou la vie privée des 
utilisateurs. 

En se concentrant sur les logiciels libres, nous souhaitons encourager avec L09 l'autonomie numérique et soutenir 
l'écosystème local en réduisant la dépendance vis-à-vis des fournisseurs de logiciels propriétaires.

## Nos services

- [SignaturePDF](https://pdf.l09.fr) : un outil permettant de travailler sur des fichiers pdf (ajout de signature,
organisation des pages, modification des métadonnées, et compression des fichiers)
- D'autres à venir bientôt

## Infrastructure

Ce repo git contient la configuration kubernetes des applications proposées.

Ce document décrit le déploiement afin que vous puissiez héberger vous même vos services de la même manière.

Nos applications sont hébergées par Scaleway, en utilisant les services suivants :

- Kubernetes Kapsule
- Managed Database for PostgreSQL

### Configuration initiale

Une fois le cluster Kunernetes démarré dans Scaleway, nous allons commencer par installer l'ingress `nginx` :

    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/cloud/deploy.yaml

Pour sécuriser les communications, nous installons aussi `cert-manager`, pour pouvoir installer des certificats https
avec Let's Encrypt

    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.yaml
    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.crds.yaml
    kubectl apply -f ./letsencrypt/


### Configuration des applications

Par la suite, chaque application contient sa configuration complète dans des fichiers `yaml` prêts à déployer dans
Kubernetes. Il suffit de lancer la commande `apply` dans un dossier pour démarrer le service associé.

    kubect apply -f ./signaturepdf

Si un service nécessite une configuration spécifique supplémentaire, ceci est indiqué dans un fichier README présent
dans son dossier.
